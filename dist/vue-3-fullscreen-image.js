import { defineComponent as O, useCssVars as D, ref as y, computed as _, onMounted as L, openBlock as a, createBlock as B, Transition as S, withCtx as A, createElementBlock as i, createElementVNode as t, normalizeClass as f, normalizeStyle as j, createCommentVNode as v, TransitionGroup as R, Fragment as U, renderList as P, pushScopeId as N, popScopeId as T, createApp as H, h as V } from "vue";
const k = (o) => (N("data-v-3909e3dd"), o = o(), T(), o), z = { class: "icons" }, M = /* @__PURE__ */ k(() => /* @__PURE__ */ t("svg", {
  xmlns: "http://www.w3.org/2000/svg",
  viewBox: "0 0 24 24",
  fill: "none",
  stroke: "currentColor",
  "stroke-width": "2",
  "stroke-linecap": "round",
  "stroke-linejoin": "round"
}, [
  /* @__PURE__ */ t("path", { d: "M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4" }),
  /* @__PURE__ */ t("polyline", { points: "17 8 12 3 7 8" }),
  /* @__PURE__ */ t("line", {
    x1: "12",
    y1: "3",
    x2: "12",
    y2: "15"
  })
], -1)), q = [
  M
], G = /* @__PURE__ */ k(() => /* @__PURE__ */ t("svg", {
  xmlns: "http://www.w3.org/2000/svg",
  viewBox: "0 0 24 24",
  fill: "none",
  stroke: "currentColor",
  "stroke-width": "2",
  "stroke-linecap": "round",
  "stroke-linejoin": "round"
}, [
  /* @__PURE__ */ t("line", {
    x1: "18",
    y1: "6",
    x2: "6",
    y2: "18"
  }),
  /* @__PURE__ */ t("line", {
    x1: "6",
    y1: "6",
    x2: "18",
    y2: "18"
  })
], -1)), K = [
  G
], W = ["disabled"], J = /* @__PURE__ */ k(() => /* @__PURE__ */ t("svg", {
  xmlns: "http://www.w3.org/2000/svg",
  viewBox: "0 0 24 24",
  fill: "none",
  stroke: "currentColor",
  "stroke-width": "2",
  "stroke-linecap": "round",
  "stroke-linejoin": "round"
}, [
  /* @__PURE__ */ t("polyline", { points: "15 18 9 12 15 6" })
], -1)), Q = [
  J
], X = ["src", "alt"], Y = ["disabled"], Z = /* @__PURE__ */ k(() => /* @__PURE__ */ t("svg", {
  xmlns: "http://www.w3.org/2000/svg",
  viewBox: "0 0 24 24",
  fill: "none",
  stroke: "currentColor",
  "stroke-width": "2",
  "stroke-linecap": "round",
  "stroke-linejoin": "round"
}, [
  /* @__PURE__ */ t("polyline", { points: "9 6 15 12 9 18" })
], -1)), $ = [
  Z
], ee = {
  key: 1,
  class: "image-container"
}, oe = ["src", "alt"], te = /* @__PURE__ */ O({
  __name: "FullscreenImage",
  props: {
    imageUrl: {},
    anchor: {},
    animation: { default: "fade" },
    imageAlt: { default: "" },
    withDownload: { type: Boolean, default: !0 },
    withClose: { type: Boolean, default: !0 },
    withFocusOnClose: { type: Boolean, default: !0 },
    withCloseOnEscape: { type: Boolean, default: !0 },
    closeOnClikOutside: { type: Boolean, default: !0 },
    maxHeight: { default: "80vh" },
    maxWidth: { default: "80vw" },
    backdropColor: { default: "rgba(0, 0, 0, 0.7)" },
    backdropBlur: { default: "1px" },
    backdropClass: { default: "" },
    backdropStyle: { default: "" }
  },
  emits: ["close"],
  setup(o, { emit: c }) {
    D((e) => ({
      55373430: e.backdropBlur,
      "51bea4ba": e.backdropColor,
      "5ffcc390": e.maxHeight
    }));
    const l = o, u = c, n = y(0), s = y(!1), d = () => {
      s.value = !0, setTimeout(() => {
        u("close");
      }, 500);
    }, m = async (e) => {
      e.preventDefault(), e.stopPropagation();
      try {
        const p = Array.isArray(l.imageUrl) ? l.imageUrl[n.value] : l.imageUrl, g = await (await fetch(p)).blob(), r = document.createElement("a");
        r.href = window.URL.createObjectURL(g), r.download = "image", r.style.display = "none", document.body.appendChild(r), r.click(), document.body.removeChild(r), window.URL.revokeObjectURL(r.href);
      } catch (p) {
        console.error("Error downloading image:", p);
      }
    }, I = (e) => {
      l.withCloseOnEscape && e.key === "Escape" && d();
    }, x = () => {
      l.closeOnClikOutside && d();
    }, b = _(() => n.value === 0), C = _(() => n.value === l.imageUrl.length - 1), E = () => {
      n.value--;
    }, F = () => {
      n.value++;
    }, h = y();
    return L(() => {
      l.withFocusOnClose && h.value && h.value.focus();
    }), (e, p) => (a(), B(S, {
      name: e.animation,
      appear: ""
    }, {
      default: A(() => [
        e.imageUrl && !s.value ? (a(), i("div", {
          key: 0,
          class: "fullscreen-image",
          onKeydown: I,
          tabindex: "0",
          role: "dialog",
          "aria-modal": "true",
          "aria-label": "Fullscreen Image"
        }, [
          t("div", {
            class: f(["backdrop", e.backdropClass]),
            onClick: x,
            style: j(e.backdropStyle)
          }, [
            t("div", z, [
              e.withDownload ? (a(), i("button", {
                key: 0,
                tabindex: "0",
                class: "icon download-icon",
                onClick: m,
                title: "close"
              }, q)) : v("", !0),
              e.withClose ? (a(), i("button", {
                key: 1,
                ref_key: "closeButtonRef",
                ref: h,
                tabindex: "0",
                class: "icon close-icon",
                onClick: d,
                title: "download"
              }, K, 512)) : v("", !0)
            ])
          ], 6),
          Array.isArray(e.imageUrl) ? (a(), B(R, {
            key: 0,
            name: "list",
            tag: "div",
            class: "image-container"
          }, {
            default: A(() => [
              t("button", {
                key: "previouus",
                disabled: b.value,
                class: f(["icon", b.value && "icon--disabled"]),
                onClick: E
              }, Q, 10, W),
              (a(!0), i(U, null, P(e.imageUrl, (w, g) => (a(), i(U, {
                key: w + g
              }, [
                n.value === g ? (a(), i("img", {
                  key: 0,
                  src: w,
                  alt: Array.isArray(e.imageAlt) ? e.imageAlt[n.value] : e.imageAlt
                }, null, 8, X)) : v("", !0)
              ], 64))), 128)),
              t("button", {
                key: "neeext",
                disabled: C.value,
                class: f(["icon", C.value && "icon--disabled"]),
                onClick: F
              }, $, 10, Y)
            ]),
            _: 1
          })) : (a(), i("div", ee, [
            t("img", {
              src: e.imageUrl,
              alt: Array.isArray(e.imageAlt) ? e.imageAlt[n.value] : e.imageAlt
            }, null, 8, oe)
          ]))
        ], 32)) : v("", !0)
      ]),
      _: 1
    }, 8, ["name"]));
  }
}), ne = (o, c) => {
  const l = o.__vccOpts || o;
  for (const [u, n] of c)
    l[u] = n;
  return l;
}, le = /* @__PURE__ */ ne(te, [["__scopeId", "data-v-3909e3dd"]]), ae = {
  mounted(o, c) {
    const l = () => {
      var m;
      const n = H({
        render() {
          return V(le, { ...c.value, onClose: () => u(n, s) });
        }
      }), s = document.createElement("div"), d = document.querySelector(((m = c.value) == null ? void 0 : m.anchor) || "body");
      d && (d.appendChild(s), n.mount(s));
    }, u = (n, s) => {
      n.unmount(), s.remove();
    };
    o.style.cursor = "pointer", o.addEventListener("click", l), o.openFullscreenImage = l;
  },
  beforeUnmount(o) {
    o.removeEventListener("click", o.openFullscreenImage);
  }
};
function re(o) {
  o.directive("fullscreen-image", ae);
}
export {
  re as fullscreenImagePlugin
};
